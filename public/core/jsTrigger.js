/*
  Sets:
    Q.core.engine.__lineMatched
 */
 
matches          = typeof matches         !== 'undefined' ? matches         : []
Q                = typeof Q               !== 'undefined' ? Q               : {}

Q.triggers       = [] // !important, remake each time the file is reloaded

Q.core           = typeof Q.core          !== 'undefined' ? Q.core          : {}
Q.core.triggers  = typeof Q.core.triggers !== 'undefined' ? Q.core.triggers : {}

Q.core.triggers.ignore       = typeof Q.core.triggers.ignore       !== 'undefined' ? Q.core.triggers.ignore       : false
Q.core.triggers.matches      = typeof Q.core.triggers.matches      !== 'undefined' ? Q.core.triggers.matches      : []
Q.core.triggers.instructions = typeof Q.core.triggers.instructions !== 'undefined' ? Q.core.triggers.instructions : []

Q.core.triggers.batch = function(list) {
  for (var i = 0; i < list.length; i++) {
    var m = list[i]
    for (var j = 0; j < m.patterns.length; j++) {
      Q.core.triggers.make(m.patterns[j], m.code, m.name)
    }
  }
}

Q.core.triggers.eval = function(str) {
  var out  = []
  var temp = str.split('+trigger')
  temp.shift()
  for (var i = 0; i < temp.length; i++) {
    var name     = ''
    var patterns = []
    var code     = ''
    var codeM    = temp[i].match(/\+do[\s\S]*?\-en/gim)
    if (codeM) { for (var j = 0; j < codeM.length; j++) { code = codeM[j].replace('+do','').replace('-en','') } }
    var nameM    = temp[i].match(/\+n[\s\S]*?\r?\n/g)
    if (nameM) { for (var j = 0; j < nameM.length; j++) { name = nameM[j].replace('+n ','').replace('\n','') } }
    var pattM    = temp[i].match(/\+p[\s\S]*?\r?\n/g)
    if (pattM) { for (var j = 0; j < pattM.length; j++) { patterns.push(pattM[j].replace('\n','').replace('+p ','')) } }
    out.push({name: name, patterns: patterns, code: code})
  }
  return out
}

Q.core.triggers.make = function(pattern, response, name, uuid) {
  var RE   = new RegExp(pattern)
  var uuid = uuid || Q.core.utilities.uuid()
  Q.triggers.push({name: name, response: response, pattern: pattern, uuid: uuid, RE: RE})
}

// Trigger Matching Function
Q.core.triggers.discover = function(line) {
  var tr  = Q.triggers
  var out = undefined
  for (var i = 0; i < tr.length; i++) {
    var t = tr[i]
    if (t.RE && t.RE.exec(line)) { // if the RegExp is not pre-formed, writing them on each loop is expensive
      if (Q.core.engine.__lineMatched == false) { // ! Engine
        if (Q.core.triggers.ignore) { Q.core.triggers.ignore = false } else { Q.core.engine.__lineMatched = true }
      }
      Q.core.triggers.matches = t.RE.exec(line)
      matches = Q.core.triggers.matches
      try { out = eval(t.response) } catch(err) { console.log(err.message) }
      tr[i].matched  = tr[i].matched || 0
      tr[i].matched += 1
    }
  }
}

Q.core.triggers.make(
  '^[\\S\\s]*([Hh]ttps?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)?)[\\S\\s]*$',
  `
    Q.core.triggers.ignore = true
    var url = matches[1]
    sub({fx: 'internal', what: url, so: '<a href="'+url+'" target="_blank">'+url+'</a>'})
  `,
  'http.link')
  
Q.core.triggers.make(
  '^You will TIMEOUT in 1 minute unless you do something\.$',
  `Q.core.send('p snake')`,
  'TIMEOUT')

sub = function(options) {
  Q.core.engine.__lineModify = true
  var options = options || {}
  if (options.fx == 'whole-sub') {
    Q.core.triggers.instructions.push({fx: 'whole-sub', options: options})
  } else if (options.fx == 'internal') {
    Q.core.triggers.instructions.push({fx: 'internal',  options: options})
  } else {
    Q.core.triggers.instructions.push({fx: 'deletion',  options: options})
  }
}

/*
// UI functions
highlight = function(name, color, multi) {
 jv.lineModify = true
 var newSpan = '<span style="color: '+color+';">'
 var endSpan = '</span>'
 jt.mods.push({ pattern: name, fx: 'replace', args: [newSpan + name + endSpan] })
}

multisub = function(phrase, newpattern) {
 jv.lineModify = true
 var RE = new RegExp(phrase,'g')
 jt.mods.push({ fx: 'multireplace', regex: RE, newpattern: newpattern })
}
tagOn = function(s) {
 jv.lineModify = true
 jt.mods.push({ fx: 'append', args: [s] })
}
*/