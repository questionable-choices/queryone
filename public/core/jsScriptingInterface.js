Q               = typeof Q               !== 'undefined' ? Q               : {}
Q.core          = typeof Q.core          !== 'undefined' ? Q.core          : {}
Q.core.vars     = typeof Q.core.vars     !== 'undefined' ? Q.core.vars     : {}

Q.core.settings = typeof Q.core.settings !== 'undefined' ? Q.core.settings : {}

Q.core.settings.scriptingDisplay = 'Modular'

Q.core.scriptingWindow = function() {
  $('#scripting-main').remove()
  var d  = ''
      d += '<div id="scripting-main">'
      d += '<div id="scripting-container">'
      d +=   '<div id="scripting-header">'
      d +=     '<span class="scripting-heading">Scripting</span>'
      d +=     '<div id="scripting-closing" onclick="Q.core.scriptingClose()"><span class="scripting-closing">x</span></div>'
      d +=   '</div>'
      d +=   '<div id="scripting-navigator-resizing">'
      d +=   '<div id="scripting-navigator">'
      // d +=     '<div id="scripting-navstyle">'
      // d +=       '<div id="scripting-navstyle-modular" class="QO-scripting-navstyle" onclick="Q.core.scriptingRender(\'Modular\')">Modular</div>'
      // d +=       '<div id="scripting-navstyle-class" class="QO-scripting-navstyle" onclick="Q.core.scriptingRender(\'Class\')">Class</div>'
      // d +=       '<div id="scripting-navstyle-toplevel" class="QO-scripting-navstyle" onclick="Q.core.scriptingRender(\'Top Level\')">Top Level</div>'
      // d +=     '</div>'
      d +=     '<div id="QO-scripting-addModule">'
      d +=       '<span class="QO-scripting-addModule">Add Module</span>'
      d +=       '<div id="QO-scripting-addModule-top" class="QO-scripting-addModule top" onclick="Q.core.scripting.addModule(\'top\')">&andv;</div>'
      d +=       '<div id="QO-scripting-addModule-bottom" class="QO-scripting-addModule bottom" onclick="Q.core.scripting.addModule(\'bottom\')">&orv;</div>'
      d +=     '</div>'
      d +=     '<div id="scripting-navigator-content">'
      d +=     '</div>'
      d +=     '<div id="scripting-separator"></div>'
      d +=   '</div></div>'
      d +=   '<div id="scripting-body">'
      d +=   '</div>'
      d += '</div>'
      d += '</div>'
  $('#container').append(d)
  $('#scripting-main').draggable({handle: '#scripting-header', scroll: false})
  $('#scripting-container').resizable({
      maxHeight: 500,
      maxWidth : 650,
      minHeight: 300,
      minWidth : 410,
    });
  $('#scripting-navigator').resizable({
      minHeight: '100%',
      maxHeight: '100%',
      maxWidth : '50%',
      minWidth : 140,
    });
  Q.core.scriptingRender(Q.core.settings.scriptingDisplay)
  Q.core.scripting.readObjects()
  document.getElementById('scripting-main').addEventListener('mouseenter', function() { $('body').trigger('script-moused') });
  document.getElementById('scripting-main').addEventListener('mouseleave',   function() { $('body').trigger('script-unmoused')});
  $('#panel-scripts').addClass('active')
}

Q.core.scriptingClose = function() {
  $('#scripting-main').remove()
  $('#panel-scripts').removeClass('active')
}

Q.core.scriptingToggle = function() {
  if ($('#scripting-main').length) {
    Q.core.scriptingClose()
  } else {
    Q.core.scriptingWindow()
  }
}

Q.core.scriptingRender = function(style) {
  Q.core.settings.scriptingDisplay = style
  $('.QO-scripting-navstyle').removeClass('active')
  switch(style) {
    case 'Modular':
      Q.core.scriptingRenderModular()
      $('#scripting-navstyle-modular').addClass('active')
      break;
    case 'Class':
      $('#scripting-navstyle-class').addClass('active')
      break;
    case 'Top Level':
      $('#scripting-navstyle-toplevel').addClass('active')
      break;
  }
}

Q.core.scriptingRenderModular = function() {
  $('.QO-nav-topdir').remove()
  var out   = []
  var SEQ   = Q.git.__sequences
  var RET   = Q.git.__forRetrieval
  var SCR   = Q.scripts
  var TRI   = Q.triggers
  var ALI   = Q.aliases
  var lpad  = Q.core.utilities.lpad
  
  for (var i = 0; i < SEQ.length; i++) {
    var str = ''
    var key = SEQ[i].key
    for (var j = 0; j < RET.length; j++) {
      var a = ''
      var t = RET[j]
      if (t._Qkey == key) {
        var d = ''
        // locate all scripts, triggers & aliases from this parent
        //   scripts
        for (var k = 0; k < SCR.length; k++) {
          var u = SCR[k]
          if (u.parent == t.id) {
            d += '<div id="' + u.uuid + '" class="QO-nav-item script" onclick="Q.core.scriptingShowCode(\'' + u.uuid + '\')">'
            d += '<div id="' + u.uuid + '-prepend" class="QO-nav-itemPrepend"></div>'
            d += '<div id="' + u.uuid + '-display" class="QO-nav-itemDisplay">' + lpad('',4,' ') + u.name + '</div>'
            d += '</div>'
          }
        }
        // if (d != '') { d = '<div id="QO-navDisplay-' + key + '-scripts" class="QO-nav-scripts">  scripts</div>' + d }
        a += d; d = ''
        //  triggers
        for (var k = 0; k < TRI.length; k++) {
          var u = TRI[k]
          if (u.parent == t.id) {
            d += '<div id="' + u.uuid + '" class="QO-nav-item trigger" onclick="Q.core.scriptingShowCode(\'' + u.uuid + '\')">'
            d += '<div id="' + u.uuid + '-prepend" class="QO-nav-itemPrepend"></div>'
            d += '<div id="' + u.uuid + '-display" class="QO-nav-itemDisplay">' + lpad('',4,' ') + u.name + '</div>'
            d += '</div>'
          }
        }
        // if (d != '') { d = '<div id="QO-navDisplay-' + key + '-triggers" class="QO-nav-triggers">  triggers</div>' + d }
        a += d; d = ''
        //  aliases
        for (var k = 0; k < ALI.length; k++) {
          var u = ALI[k]
          if (u.parent == t.id) {
            d += '<div id="' + u.uuid + '" class="QO-nav-item alias" onclick="Q.core.scriptingShowCode(\'' + u.uuid + '\')">'
            d += '<div id="' + u.uuid + '-prepend" class="QO-nav-itemPrepend"></div>'
            d += '<div id="' + u.uuid + '-display" class="QO-nav-itemDisplay">' + lpad('',4,' ') + u.name + '</div>'
            d += '</div>'
          }
        }
        // if (d != '') { d = '<div id="QO-navDisplay-' + key + '-aliases" class="QO-nav-aliases">  aliases</div>' + d }
        a += d; d = ''
        if (a != '') {
          str += '<div id="QO-navSubfolder-' + t._Qsubdir + '" class="QO-navSubfolder"> ' + t._Qsubdir + '</div>' + a
        }
      }
    }
    if (str != '') { 
      var out  = ''
          out += '<div id="QO-nav-' + key + '" class="QO-nav-topdir">'
          out +=   '<div id="QO-navDisplay-' + key + '" class="QO-nav-topdirDisplay">' + key + '</div>'
          out +=   str
          out += '</div>' 
      $('#scripting-navigator-content').append(out)
    }
  }
}


Q.registry = typeof Q.registry != 'undefined' ? Q.registry : {}
Q.objects  = typeof Q.objects  != 'undefined' ? Q.objects  : []
Q.core.scripting = typeof Q.core.scripting != 'undefined' ? Q.core.scripting : {}
Q.core.scripting.__lastUUID = ''

Q.core.scripting.readObjects = function() {
  var lastUUID = ''
  if (Q.core.scripting.__lastUUID != '') { lastUUID = Q.core.scripting.__lastUUID }

  // Collect all Modules
  var modules = []
  var nonmodules = []
  for (var i = 0; i < Q.objects.length; i++) {
    var item = Q.objects[i]
    if (item.type != 'module') {
      nonmodules.push(item)
    } else {
      modules.push(item)
    }
  }

  for (var i = 0; i < modules.length; i++) {
    var module = modules[i]
    var toFocus = true
    if (i == modules.length - 1) {
      toFocus = false
    }
    Q.core.scripting.addModule('bottom', module.uuid, module, toFocus)
  }
  for (var i = 0; i < nonmodules.length; i++) {
    var object = nonmodules[i]
    var toFocus = true
    if (i == nonmodules.length - 1) {
      toFocus = false
    }
    Q.core.scripting.addObject(object.parent, object.uuid, object, toFocus)
  }

  if (lastUUID != '') {
    Q.core.scripting.focusObject(lastUUID)
  }
}

Q.core.scripting.updateElement = function(uuid, key, data) {
  var updated = false
  for (var i = 0; i < Q.objects.length; i++) {
    if (Q.objects[i].uuid == uuid) {
      if (typeof Q.objects[i][key] != 'undefined') {
        Q.objects[i][key] = data
        updated = true
        break
      }
    }
  }
  return updated
}

Q.core.scripting.addModule = function(sequence, u, options, noFocus) {
  var u = u || Q.core.utilities.uuid()
  var m = ''
  if (typeof options != 'undefined' && options.name != '') { m = options.name }
  var d = ''
  d += '<div id="' + u + '" class="QO-scripting-module">'
  d +=   '<div id="' + u + '-header" class="QO-scripting-moduleHeader">'
  d +=   '<input id="" class="QO-scripting-module-name" placeholder="Module Name here" onfocus="Q.core.scripting.focusObject(\'' + u + '\')" value="' + m + '"></input>'
  d +=   '<div id="" class="QO-scripting-addObject" onclick="Q.core.scripting.addObject(\'' + u + '\')">+</div>'
  d +=   '</div>'
  d +=   '<div id="' + u + '-sortable-area" class="QO-scripting-module-sortableArea">'
  d +=     '<div id="" class="QO-scripting-object fakeObject" style="height: 3px;"></div>' // !important to allow draggable in empty modules, not sure why
  d +=   '</div>'
  d += '</div>'
  // FIX THIS
  if (sequence == 'top') {
    $('#scripting-navigator-content').prepend(d)
    if (options) { Q.core.scripting.addElement(u, options) } else {
      Q.core.scripting.addElement(u, {type: 'module', order: 'reverse'})
    }
  } else {
    $('#scripting-navigator-content').append(d)
    if (options) { Q.core.scripting.addElement(u, options) } else {
      Q.core.scripting.addElement(u, {type: 'module'})
    }
  }

  Q.core.scripting.organiseObjects()
  if (!noFocus) { Q.core.scripting.focusObject(u) }
  return u
}

// Add Object > Element
Q.core.scripting.addObject = function(uuid, u, options, noFocus) {
  var u = u || Q.core.utilities.uuid()
  var m = ''
  var type = 'script'
  if (typeof options != 'undefined') {
    if (options.name != '') { m = options.name }
    if (options.type != 'script') { type = options.type }
  }
  var d = ''
  d += '<div id="' + u + '" class="QO-scripting-object ' + type + '">'
  d +=   '<div id="" class="QO-scripting-object-prepend" onclick="Q.core.scripting.mutateObject(\'' + u + '\')"></div>'
  d +=   '<input id="" class="QO-scripting-object-name" placeholder="Object name here" onfocus="Q.core.scripting.focusObject(\'' + u + '\')" value="' + m + '"></input>'
  d +=   '<div id="" class="QO-scripting-object-postpend QO-scripting-addObject" onclick="Q.core.scripting.editObject(\'' + u + '\')">&gtcir;</div>'
  d += '</div>'
  $('#' + uuid + ' .QO-scripting-module-sortableArea').append(d)
  if (options) {
    Q.core.scripting.addElement(u, options)
  } else {
    Q.core.scripting.addElement(u, {parent: uuid, type: 'script'})
  }

  Q.core.scripting.organiseObjects()
  if (!noFocus) { Q.core.scripting.focusObject(u) }
  return u
}

Q.core.scripting.organiseObjects = function() {
  // Sort Modules
  $('#scripting-navigator-content').sortable({
    axis: 'y',
    containment: 'parent',
    items: '.QO-scripting-module',
  })
  $('#scripting-navigator-content').on('sortstop', function(event, ui) {
    var arrangement = $('#scripting-navigator-content').sortable('toArray')
    Q.objects.sort(Q.core.utilities.sortChain([
      Q.core.utilities.sortBy('uuid', true, function(a) { return arrangement.indexOf(a) }),
    ]))
  })

  // Sorting
  /* https://stackoverflow.com/a/4896227 */
  Q.core.scripting.oldList  = {}
  Q.core.scripting.newList  = {}
  Q.core.scripting.listItem = {}
  $('.QO-scripting-module-sortableArea').sortable({
    start : function(event, ui) { 
              Q.core.scripting.listItem = ui.item; 
              Q.core.scripting.newList = Q.core.scripting.oldList = ui.item.parent().parent() },
    stop  : function(event, ui) {
              // Q.core.scripting.listItem[0].parent = Q.core.utilities.clone(Q.core.scripting.newList.attr('id'))
              var newParentID = Q.core.scripting.newList.attr('id')
              for (var i = 0; i < Q.objects.length; i++) {
                if (Q.objects[i].uuid == Q.core.scripting.listItem[0].id) {
                  Q.objects[i].parent = Q.core.scripting.newList.attr('id')
                }
              }
              console.log('Moved ' + Q.core.scripting.listItem[0].id + ' to  ' + Q.core.scripting.newList.attr('id') + '.')
              // Rearrange the underlying object
              //   Assessment
              var arrangement = []
              var m = document.getElementById(newParentID).getElementsByClassName('QO-scripting-module-sortableArea')[0].getElementsByClassName('QO-scripting-object')
              for (var i = 0; i < m.length; i++) {
                var element = m[i].getElementsByTagName('input')[0]
                if (element) { // excludes fakeObject (which is our spacer for jQUery.sortable)
                 // console.log(m[i].id)
                 // console.log(element.className)
                 // console.log(element.value)
                 arrangement.push(m[i].id)
                }
              }
              //   Arrangement
              Q.objects.sort(Q.core.utilities.sortChain([
                Q.core.utilities.sortBy('uuid', true, function(a) { return arrangement.indexOf(a) }),
              ]))
            },

    change: function(event, ui) { 
              if (ui.sender) { Q.core.scripting.newList = ui.placeholder.parent().parent(); } },
    connectWith: '.QO-scripting-module-sortableArea',
  }).disableSelection()
}

Q.core.scripting.addElement = function(uuid, options) {
  var options = options || {}
  var item = {}
  if (options.uuid) {
    console.log('Item #' + options.uuid + ' already exists.')
  } else {
      item.uuid     = uuid
      item.file     = options.file     || ''
      item.parent   = options.parent   || ''
      item.name     = options.name     || ''
      item.type     = options.type     || 'script'
      item.pattern  = options.pattern  || ''
      item.response = options.response || ''  // triggers
      item.output   = options.output   || ''  // aliases
      item.script   = options.script   || ''
  if (typeof options.parent == 'undefined') {
    item.parent = $('#' + uuid).parent().attr('id')
  }
  if (options && options.order == 'reverse') {
    Q.objects.unshift(item)
  } else {
    Q.objects.push(item)
  }
  }
}

// Mutate Object > Element
Q.core.scripting.mutateObject = function(uuid) {
  var r        = Q.core.scripting.mutateElement(uuid)
  var item     = r[0]
  var postpend = r[1]
  $('#' + item.uuid).addClass(item.type)
  $('#' + item.uuid + ' .QO-scripting-object-postpend').html(postpend)
  if (item.type == 'folder') {
    $('#' + item.uuid).append('<div id="' + item.uuid + '-sortableArea" class="QO-scripting-object-sortableArea fakeObject" style="height: 2px;"></div>')
  } else {
    $('#' + item.uuid).remove('.QO-scripting-object-sortableArea')
  }
  Q.core.scripting.focusObject(item.uuid)
}

Q.core.scripting.mutateElement = function(uuid) {
  var item = {}
  for (var i = 0; i < Q.objects.length; i++) {
    var t = Q.objects[i]
    if (t.uuid == uuid) {
      item = t
      break
    }
  }
  $('#' + item.uuid).removeClass(item.type)
  var postpend = '&gtcir;'
  if (item.type == 'script') {
    item.type = 'trigger'
  } else if (item.type == 'trigger') {
    item.type = 'alias'
  } else if (item.type == 'alias') {
    item.type = 'script'
    // postpend  = '+'
  } else {
    item.type = 'script'
  }
  return [item, postpend]
}

// Delete Object > Element
Q.core.scripting.deleteObject = function(uuid) {

  var forRemoval = Q.core.scripting.deleteElement(uuid) // from Javascript Object
  for (var i = 0; i < forRemoval.length; i++) {
    for (var j = Q.objects.length - 1; j > -1; j--) {
      if (forRemoval[i] == Q.objects[j].uuid) {
        var m = Q.objects.splice(j, 1)
      }
    }
  }

  $('#' + uuid).remove()
  $('#scripting-body').empty()
}

Q.core.scripting.deleteElement = function(uuid) {
  var arr = []
      arr.push(uuid)
  for (var i = 0; i < Q.objects.length; i++) {
    if (Q.objects[i].parent == uuid) {
      arr = arr.concat(Q.core.scripting.deleteElement(Q.objects[i].uuid))
    }
  }
  return arr
}

// Focus Object
Q.core.scripting.focusObject = function(uuid) {
  $('#scripting-body').empty()
  // console.log(uuid)
  var aceBody  = 'QO-scripting-body-contained'
  var aceMode  = 'ace/mode/javascript'
  var aceTheme = 'ace/theme/tomorrow_night_eighties'

  let item = {}
  for (var i = 0; i < Q.objects.length; i++) {
    var t = Q.objects[i]
    if (t.uuid == uuid) {
      item = t
      break
    }
  }

  Q.core.scripting.__lastUUID = uuid

  var d = ''
  d += '<div id="QO-scripting-body-' + uuid + '" class="QO-scripting-body-content">'
  d +=   '<div id="QO-scripting-body-header">'
  d +=     '<div id="QO-scripting-body-name">'
  d +=       '<span class="QO-scripting-body-name-header">Name: </span>'
  d +=       '<input id="QO-scripting-body-name-actual" placeholder="Object name here" value="' + item.name + '"></input>'
  d +=     '</div>'
  d +=     '<div id="QO-scripting-body-delete" onclick="Q.core.scripting.deleteObject(\'' + uuid + '\')"><span>Delete</span></div>'
  if (item.type == 'trigger' || item.type == 'alias') {
  d +=     '<div id="QO-scripting-body-pattern">'
  d +=       '<span class="QO-scripting-body-pattern-header">Pattern: </span>'
  d +=       '<input id="QO-scripting-body-pattern-actual" placeholder="Pattern here" value="' + item.pattern + '"></input>'
  d +=     '</div>'
  }
  d +=   '</div>'
  d +=   '<div id="QO-scripting-body-contained"></div>'
  d += '</div>'
  $('#scripting-body').append(d)

  if (typeof ace != 'undefined') {
    Q.codeAce = ace.edit(aceBody)
    Q.codeAce.setTheme(aceTheme)
    Q.codeAce.session.setMode(aceMode)
    var script = 'script'
    if (item.type == 'trigger') { script = 'response' }
    if (item.type == 'alias'  ) { script = 'output'   }
    Q.codeAce.session.setValue(item[script])
  }

  // Link Script Body to Navigator
  $('#QO-scripting-body-name-actual').keyup(function(e) {
    var name = document.getElementById('QO-scripting-body-name-actual').value
    var uuid = document.getElementsByClassName('QO-scripting-body-content')[0].id
        uuid = uuid.replace('QO-scripting-body-','')
    var m    = document.getElementById(uuid + '-header')
    if (m) {
        document.getElementById(uuid + '-header').getElementsByTagName('input')[0].value = name
    } else {
        document.getElementById(uuid).getElementsByTagName('input')[0].value = name
    }
    Q.core.scripting.updateElement(uuid, 'name', name)
  })
  // Link 'Pattern' to Underlying
  $('#QO-scripting-body-pattern-actual').keyup(function(e) {
    var regex = document.getElementById('QO-scripting-body-pattern-actual').value
    Q.core.scripting.updateElement(uuid, 'pattern', regex)
  })
  // Link 'Script' to Underlying
  // let vuid = uuid
  Q.codeAce.session.on('change', function(e) {
    var toChange = 'script'
    var content  = Q.codeAce.session.getValue()
    if (item.type == 'trigger') { toChange = 'response' }
    if (item.type == 'alias'  ) { toChange = 'output'   }
    Q.core.scripting.updateElement(uuid, toChange, content)
  })

  if (item.type == 'module') {
    $('#' + uuid + '-header input').keyup(function(e) {
      var name = document.getElementById(uuid + '-header').getElementsByTagName('input')[0].value
      document.getElementById('QO-scripting-body-name-actual').value = name
      Q.core.scripting.updateElement(uuid, 'name', name)
    })
  } else {
    $('#' + uuid + ' input').keyup(function(e) {
      var name = document.getElementById(uuid).getElementsByTagName('input')[0].value
      document.getElementById('QO-scripting-body-name-actual').value = name
      Q.core.scripting.updateElement(uuid, 'name', name)
    })
  }
}

Q.core.scripting.compress = function() {
  
}

Q.core.scripting.decompress = function() {

}