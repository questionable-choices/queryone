Q                = typeof Q               !== 'undefined' ? Q               : {}
Q.gmcp           = typeof Q.gmcp          !== 'undefined' ? Q.gmcp          : {}
Q.core           = typeof Q.core          !== 'undefined' ? Q.core          : {}
Q.core.engine    = typeof Q.core.engine   !== 'undefined' ? Q.core.engine   : {}

Q.core.engine._logGMCP = typeof Q.core.engine._logGMCP !== 'undefined' ? Q.core.engine._logGMCP : false

Q.core.engine.__Buffer      = typeof Q.core.engine.__Buffer      !== 'undefined' ? Q.core.engine.__Buffer      : 0
Q.core.engine.__GABuffer    = typeof Q.core.engine.__GABuffer    !== 'undefined' ? Q.core.engine.__GABuffer    : []
Q.core.engine.__GAIAC       = typeof Q.core.engine.__GAIAC       !== 'undefined' ? Q.core.engine.__GAIAC       : false
Q.core.engine.__GASender    = typeof Q.core.engine.__GASender    !== 'undefined' ? Q.core.engine.__GASender    : []
Q.core.engine.__gmcps       = typeof Q.core.engine.__gmcps       !== 'undefined' ? Q.core.engine.__gmcps       : []
Q.core.engine.__handshake   = typeof Q.core.engine.__handshake   !== 'undefined' ? Q.core.engine.__handshake   : 0
Q.core.engine.__keepAlive   = typeof Q.core.engine.__keepAlive   !== 'undefined' ? Q.core.engine.__keepAlive   : 0
Q.core.engine.__lastpacket  = typeof Q.core.engine.__lastpacket  !== 'undefined' ? Q.core.engine.__lastpacket  : ''
Q.core.engine.__lastpackets = typeof Q.core.engine.__lastpackets !== 'undefined' ? Q.core.engine.__lastpackets : []
Q.core.engine.__lineMatched = typeof Q.core.engine.__lineMatched !== 'undefined' ? Q.core.engine.__lineMatched : false
Q.core.engine.__lineModify  = typeof Q.core.engine.__lineModify  !== 'undefined' ? Q.core.engine.__lineModify  : false
Q.core.engine.__orphanMatch = typeof Q.core.engine.__orphanMatch !== 'undefined' ? Q.core.engine.__orphanMatch : ''
Q.core.engine.__performance = typeof Q.core.engine.__performance !== 'undefined' ? Q.core.engine.__performance : 0
Q.core.engine.__pingStart   = typeof Q.core.engine.__pingStart   !== 'undefined' ? Q.core.engine.__pingStart   : 0
Q.core.engine.__pingTime    = typeof Q.core.engine.__pingTime    !== 'undefined' ? Q.core.engine.__pingTime    : 0
Q.core.engine.__pingLastReq = typeof Q.core.engine.__pingLastReq !== 'undefined' ? Q.core.engine.__pingLastReq : 0
Q.core.engine.__rawLine     = typeof Q.core.engine.__rawLine     !== 'undefined' ? Q.core.engine.__rawLine     : ''
Q.core.engine.__listenOhmap = typeof Q.core.engine.__listenOhmap !== 'undefined' ? Q.core.engine.__listenOhmap : false
Q.core.engine.__ohmap       = typeof Q.core.engine.__ohmap       !== 'undefined' ? Q.core.engine.__ohmap       : []
Q.core.engine.__gagOhmap    = typeof Q.core.engine.__gagOhmap    !== 'undefined' ? Q.core.engine.__gagOhmap    : true

Q.core.engine.__htmlEscapes = [
  {regex: /\&amp\;/g,  character: '&'},
  {regex: /\&lt\;/g,   character: '<'},
  {regex: /\&gt\;/g,   character: '>'},
  {regex: /\&quot\;/g, character: '"'},
  {regex: /\&#39\;/g,  character: "'"},
]

Q.core.engine.simplifyLine = function(line) {
 var s = line
     s = s.replace(/\<a href.+?>/g,'')
          .replace(/<\/a\>/g,'')
          .replace(/\<span .+?\>/g,'')
          .replace(/\<\/span\>/g,'')
          .replace(/\x1B\[((\d*);){0,2}(\d*)m/g,'')
          .replace(/\x1B\[[\d;]+m/g, '')
          .replace(/\x1B\[dz/g, '')
          .replace(/\x1B/g, '')
          .replace(/\\u001b/g,'')
          .replace(/\\u0001/g,'')
          .replace(/\r/g,'')
          .replace(/\x01/g, '')
          .replace(/\[4z/g, '')
 return s
}
Q.core.engine.subarray = function(arr, subarr, from_index) {
  var i = from_index >>> 0,
     sl = subarr.length,
      l = arr.length + 1 - sl;
  loop: for(; i<1; i++) {
    for (var j = 0; j < sl; j++) {
      if (arr[i+j] !== subarr[j]) { continue loop; return i; }
    }
  }
  return -1
}
Q.core.engine.lookupUint8 = function(Char) {
  var ascii = function(a){ return a.charCodeAt(0) }
  if (Q.core.engine.__mapping[Char]) { return Q.core.engine.__mapping[Char] }
  if (parseInt(Char)) { return parseInt(Char) }
  return Char.split('').map(ascii)
}
Q.core.engine.adonize  = function(str) { var out = []; out = str.split('-'); out = out.map(Q.core.engine.lookupUint8); out = [].concat.apply([], out); return out }
Q.core.engine.contains = function(cmd) { if (Q.core.engine.subarray(Q.core.engine.__GABuffer, Q.core.engine.adonize(cmd))) { return true }; return false }
          
Q.core.engine.__textFormats = {
   0:  'normal',
   1: 'bold',
   4: 'underline',
  30: 'black',
  31: 'red',
  32: 'green',
  33: 'yellow',
  34: 'blue',
  35: 'magenta',
  36: 'cyan',
  37: 'normal',
  40: 'black-bg',
  41: 'red-bg',
  42: 'green-bg',
  43: 'yellow-bg',
  44: 'blue-bg',
  45: 'magenta-bg',
  46: 'cyan-bg',
  47: 'white-bg',
}
Q.core.engine.__mapping    = {
  IAC  : 255,
  WILL : 251,
  DO   : 253,
  GMCP : 201,
  TT   :  24,
  SB   : 250,
  SE   : 240,
  GA   : 249,
  ETX  :   3,
  EOT  :   4,
  WONT : 252,
  DONT : 254,
  ATCP : 200,
 /*
  IS   :   0,
  SEND :   1,
  CR   :  13,
  ZMP  :  93, ? square bracket
  */
}
Q.core.engine.__mappedKeys = [255,251,253,201,24,250,240,249,3,4,252,254,200]

Q.core.engine.receive = function(e) {
  // console.log(e)
  Q.core.engine.__performance = new Date().getTime()
  var chunk = new Uint8Array(e.data)
  var ws    = Q.core.vars.wrapSequence
  var wl    = Q.core.settings.wrapLimit
  var clean = Q.core.utilities.clean
  if (false) {
   var out = []
   var tuo = []
   var hot = ''
   var str = ''
   for (var i = 0; i < chunk.length; i++) {
      str += String.fromCharCode(chunk[i])
      if (chunk[i] == 10) {
        out.push({line: hot, original: tuo})
        hot = ''
        tuo = []
      } else {
        hot += String.fromCharCode(chunk[i])
        tuo.push(chunk[i])
      }
   }
   out.push({line: hot, original: tuo})
   // console.log(chunk); // console.log(str)
   // console.log(out)
  }
  
  // Wrap Pages
  if (Q.core.vars.wrapPage) {
    Q.core.engine.__Buffer = 0
    Q.core.vars.wrapPage   = false
    var outj = $('#output')
     var wid = outj.css('width');  wid = clean(wid);
     var hei = outj.css('height'); hei = clean(hei);
     var top = outj.css('top');    top = clean(top);
     var lef = outj.css('left');   lef = clean(lef);
    if ( $('#output-' + ws).length) { $('#output-' + ws).remove() }
    outj.attr('id', 'output-' + ws).attr('class','hidden')
    Q.core.vars.wrapSequence++
    
    if (Q.core.vars.wrapSequence > wl) { Q.core.vars.wrapSequence = 1 }
    var d = ''
    $('#container').append('<div id="output"></div>')
    Q.core.redraw()
    // Reselect output
    Q.core.__output = $('#output')
    Q.core.__output
      .css('width',  wid)
      .css('height', hei)
      .css('top',    top)
      .css('left',   lef)
    // Handle lastpackets
    for (var i = 0; i < Q.core.engine.__lastpackets.length; i++) {
      var t = Q.core.engine.__lastpackets[i]
      for (var k = 0; k < t.length; k++) {
        Q.core.print(t[k])
      }
    }
    Q.core.engine.__lastpackets = []
    fastdom.measure(function() {
      var h = document.getElementById('output').scrollHeight
      fastdom.mutate(function() {
        Q.core.__output.scrollTop(h)  // Probably safe to use Q.core.__output, as we just redefined it
      })
    })
    outj.on( 'dblclick', Q.core.doubleclickHandler)
  }
  
  // Read data
  var s = ''
  for (var i = 0; i < chunk.length; i++) {
    var Char     = chunk[i]
    var CharNext = chunk[i + 1] || null
    if (Q.core.engine.__GAIAC && Char != 249)  { Q.core.engine.__GAIAC = false }
    if (Char === 255)                          { Q.core.engine.__GAIAC = true  }

    if ((Char === 249 && Q.core.engine.__GAIAC) || (Q.core.vars.formalClose != true && CharNext == null)) {
      Q.core.engine.interpret() // !critical
      
      if (Q.core.vars.scrollback) {
        fastdom.measure( function() {
          var h = document.getElementById('addenda').scrollHeight
          fastdom.mutate(function() { $('#addenda').scrollTop(h) })
        })
      } else {
        fastdom.measure( function() {
          var h = document.getElementById('output').scrollHeight
          fastdom.mutate(function() { Q.core.__output.scrollTop(h) })
        })
      }
    } else {
      Q.core.engine.__GABuffer.push(Char)
    }
  }
  
  Q.core.vars.packets++
  $('body').trigger('QO-performance', new Date().getTime() - Q.core.engine.__performance); Q.core.engine.__performance = 0
  // Wrapping Output
  if (Q.core.engine.__Buffer > Q.core.settings.outputLimit) {
    Q.core.vars.wrapPage = true
    Q.core.print('<span class="echo">Warning: Re-rendering buffer on next packet.</span><br>')
    Q.core.print('<span class="echo">Warning: Re-rendering buffer on next packet.</span><br>')
    Q.core.print('<span class="echo">Warning: Re-rendering buffer on next packet.</span><br>')
  }
}

Q.core.engine.interpret = function() {
  var adonize = Q.core.engine.adonize
  var msgs = []
  if (Q.core.engine.contains('IAC-WILL-GMCP'))      { $('body').trigger('QO-GMCP-Negotiation') }
  if (Q.core.engine.contains('IAC-DO-TT'))          { Q.core.engine.__GASender.push(adonize('IAC-WILL-TT')) }
  if (Q.core.engine.contains('IAC-SB-TT-1-IAC-SE')) { Q.core.engine.__GASender.push(adonize('IAC-SB-TT-0-QueryOne-IAC-SE')) }
  Q.core.engine.elucidate() // !critical, strips out GMCP data
  for (var k = 0; k < Q.core.engine.__GASender.length; k++) {
    Q.core.Uint8(Q.core.engine.__GASender[k])
  }
  Q.core.engine.__GASender = []
  Q.core.engine.parse() // !critical
  Q.core.engine.__GABuffer = []
}
  
Q.core.engine.elucidate = function() {
  Q.core.engine.__listenOhmap = false // sigh
  var ohmap = []
  var form = 'IAC-SB-GMCP-message-!IAC-IAC-SE'
  /*
  var out = []
  var outPositions = []; var outTemp = '';
  */
  var gmcpFlag = false
  var buf = Q.core.utilities.clone(Q.core.engine.__GABuffer)
  var ouf = []
  var puf = []
  for (var k = 0; k < buf.length; k++) {
   var Char = buf[k]
   var CharNext = null;     if (buf[k + 1]) { CharNext     = buf[k + 1] }
   var CharNextNext = null; if (buf[k + 2]) { CharNextNext = buf[k + 2] }
   var CharBefore = null;   if (buf[k - 1]) { CharBefore   = buf[k - 1] }
   if (Char === 255 && CharNext === 250 && CharNextNext === 201) {
    gmcpFlag = true // log('IAC-SB-GMCP')
   }
   if (Char === 255 && CharNext === 240 && CharBefore != 255) {
    gmcpFlag = false // log('IAC-SE')
    Q.core.engine.readGMCP(ouf) // !important
    if (ohmap.length > 0) {
     Q.core.engine.__ohmap = Q.core.utilites.clone(ohmap)
    }
    ouf = []
   }
   if (gmcpFlag) {
    var c = Char
    if (Q.core.engine.__mappedKeys.includes(c)) { /* Ignore this key */ } else {
     c = String.fromCharCode(c)
    }
    ouf.push(c)
   } else {
    if (Q.core.engine.__listenOhmap) { ohmap.push(Char) }
    if (Q.core.engine.__listenOhmap && Q.core.engine.__gagOhmap) { /* Gagging */ } else {
    puf.push(Char) }
   }
  }
  if (false) {
    console.log(ouf)
    console.log(puf)
  }
  Q.core.engine.__GABuffer = Q.core.utilities.clone(puf)
} 

Q.core.engine.readGMCP = function(arr) {
  var s = arr.join('').replace('255250201','')
  var gm     = s.match(/^(.*?)\s/)
  var cp     = s.match(/.*?\s(.*)/)
  var update = ''
  if (gm) { update = gm[1] }
  if (gm) { gm     = gm[1] }
  if (cp) {
    cp = cp[1].trim()
    if (update == 'IRE.Display.Ohmap' && cp == '"start"') {
      Q.core.engine.__listenOhmap = true
    } else if (update == 'IRE.Display.Ohmap' && cp == '"stop"') {
      Q.core.engine.__listenOhmap = false
    }
    cp = JSON.parse(cp)
  }
  var stringToObject = function(str, type) {
    type = type || "object";  // can pass "function"
    var arr = str.split(".");
    var fn = Q.gmcp; //  global
    for (var i = 0, len = arr.length; i < len; i++) {
      fn[arr[i]] = fn[arr[i]] || {}
      fn = fn[arr[i]];
    }
    if (typeof fn !== type) {
      throw new Error(type +" not found: " + str);
    }
    return fn;
  }
  if (Q.core.engine._logGMCP) { console.log(gm); console.log(cp) }
  if (gm && cp) {
    // empty players
    if (gm == 'Room.Players')      { Q.gmcp.Room = Q.gmcp.Room || {}; Q.gmcp.Room.Players = {} }
    if (gm == 'Room.AddPlayer')    { Q.gmcp.Room = Q.gmcp.Room || {}; Q.gmcp.Room.AddPlayer = {} }
    if (gm == 'Room.RemovePlayer') { Q.gmcp.Room = Q.gmcp.Room || {}; Q.gmcp.Room.RemovePlayer = {} }
    if (gm == 'IRE.Rift.List')     { Q.gmcp.IRE  = Q.gmcp.IRE  || {}; Q.gmcp.IRE.Rift = {} }
    gm = stringToObject(gm)
    for (var i in cp) { gm[i] = cp[i] }
    var type = s.match(/^(.*?)\s/)[1]
    if (type == 'Char.Vitals') { Q.core.engine.__handshake = 2 } // ! negotiation complete
    if (type == 'char.vitals') { Q.core.engine.__handshake = 2 } // ! negotiation complete
    $('body').trigger('gmcp', update)
  }
}

Q.core.engine._matchColor     = /\&lt\;COLOR \#(\w+)\&gt\;(.*?)\&lt\;\/COLOR\&gt\;/
Q.core.engine._matchColorALL  = /\&lt\;COLOR \#(\w+)\&gt\;(.*?)\&lt\;\/COLOR\&gt\;/g
Q.core.engine._matchHref      = /\&lt\;SEND HREF=\&quot\;(.*?)\&quot\;\&gt\;(.*?)\&lt\;\/SEND\&gt\;/
Q.core.engine._matchHrefALL   = /\&lt\;SEND HREF=\&quot\;(.*?)\&quot\;\&gt\;(.*?)\&lt\;\/SEND\&gt\;/g

Q.core.engine._matchPrompt    = /\&lt\;PROMPT\&gt\;(.*?)\&lt\;\/PROMPT\&gt\;/
Q.core.engine._matchPromptALL = /\&lt\;PROMPT\&gt\;(.*?)\&lt\;\/PROMPT\&gt\;/g
Q.core.engine._matchRExits    = /\&lt\;RExits\&gt\;(.*?)\&lt\;\/RExits\&gt\;/
Q.core.engine._matchRExitsALL = /\&lt\;RExits\&gt\;(.*?)\&lt\;\/RExits\&gt\;/g
Q.core.engine._matchRName     = /\&lt\;RName\&gt\;(.*?)\&lt\;\/RName\&gt\;/
Q.core.engine._matchRNameALL  = /\&lt\;RName\&gt\;(.*?)\&lt\;\/RName\&gt\;/g
Q.core.engine._matchRNum      = /\&lt\;RNum (\d+) \/\&gt\;/
Q.core.engine._matchRNumALL   = /\&lt\;RNum (\d+) \/\&gt\;/g

Q.core.engine.parse = function() {
  var colorFlag = 0
  var data      = ''
  for (var v of Q.core.engine.__GABuffer) { 
    if (!Q.core.engine.__mappedKeys.includes(v)) {
      var n = String.fromCharCode(v)
      var htmlMap = {'&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#39;'}
      if (htmlMap[n]) { n = htmlMap[n] }
      data += n
    }
  }
  var mcolr = data.match(Q.core.engine._matchColorALL)
  if (mcolr) { for (var i = 0; i < mcolr.length; i++) {
    var m = mcolr[i]
    m = m.replace(/\&lt\;COLOR \#(\w+)\&gt\;/,'<span style="color: #$1;">').replace(/\&lt\;\/COLOR\&gt\;/,'</span>')
    data = data.replace(mcolr[i],m)
  }}
  var mhref = data.match(Q.core.engine._matchHrefALL)
  if (mhref) { for (var i = 0; i < mhref.length; i++) {
    var m = mhref[i]
    m = m.replace(Q.core.engine._matchHref,'<a href="#" onclick="Q.core.send(\'$1\')">$2</a>')
    data = data.replace(mhref[i],m)
  }}

  var mpromp = data.match(Q.core.engine._matchPromptALL)
  if (mpromp) { for (var i = 0; i < mpromp.length; i++) {
    var m = mpromp[i]
        m = m.replace(Q.core.engine._matchPrompt, '<span class="prompt">$1</span>')
    data  = data.replace(mpromp[i], m)
  }}
  var mexits = data.match(Q.core.engine._matchRExitsALL)
  if (mexits) { for (var i = 0; i < mexits.length; i++) {
    var m = mexits[i]
        m = m.replace(Q.core.engine._matchRExits, '<span class="QO-RExits">$1</span>')
    data  = data.replace(mexits[i], m)
  }}
  var mrname = data.match(Q.core.engine._matchRNameALL)
  if (mrname) { for (var i = 0; i < mrname.length; i++) {
    var m = mrname[i]
        m = m.replace(Q.core.engine._matchRName, '<span class="QO-RName">$1</span>')
    data  = data.replace(mrname[i], m)
  }}
  var mrnum = data.match(Q.core.engine._matchRNumALL)
  if (mrnum) { for (var i = 0; i < mrnum.length; i++) {
    var m = mrnum[i]
        m = m.replace(Q.core.engine._matchRNum, '<span class="QO-RNum RNum-$1"></span>')
    data  = data.replace(mrnum[i], m)
  }}
  data = data.replace(/\x01/g, '').replace(/\[4z/g, '').replace(/\r/g,'')

  var lines    = data.split(/\n/)
  var outgoing = []
  var pxn      = new Date()
  
  for (var k = 0; k < lines.length; k++) {
    var currentTime = new Date()
    var line = lines[k]
    var s    = lines[k]
    Q.core.engine.__lineMatched = false
    var q = Q.core.engine.handleTriggers(line) // !important

    // Not sure why we have orphan lines, but there you go
    if (s.match(/^\x1B\[((\d*);){0,2}(\d*)m$/g)) { 
      Q.core.engine.__orphanMatch = s
    } else if (s.match(/^$/g) && k == 0) {
      Q.core.engine.__orphanMatch = s
    } else {
      if (Q.core.engine.__orphanMatch.length > 0) { s = Q.core.engine.__orphanMatch + s; Q.core.engine.__orphanMatch = '' } 
      // Handle Colour!
      var ctags = s.match(/\x1B\[((\d*);){0,2}(\d*)m/g)
      var showTag = false
      if (ctags) {
        for (var p in ctags) {
          var m = ctags[p],
              n = m.replace('\x1B','').replace('[','').replace('m','').split(';')
          for (var j = 0; j < n.length; j++) { n[j] = Q.core.engine.__textFormats[n[j]] }
          var o = n.join(' ')
          if (Q.core.engine.__lastBGColour && !n[1]) { o = o + ' ' + Q.core.engine.__lastBGColour }
          if (showTag) {
            s = s.replace(m, m+'</span><span class="' + o + '">')
          } else {
            s = s.replace(m, '</span><span class="' + o + '">')
          }
          Q.core.engine.__lastFGColour = n[0]
          if (n[1]) { Q.core.engine.__lastBGColour = n[1] } else { Q.core.engine.__lastBGColour = null }
        }
      }
   
      // replicate jm.handleTrigger
      s = s.replace(/\x1B\[((\d*);){0,2}(\d*)m/g,'')
           .replace(/\x1B\[[\d;]+m/g, '')
           .replace(/\x1B\[dz/g, '')
           .replace(/\x1B/g, '')
           .replace(/\\u001b/g,'')
           .replace(/\\u0001/g,'')
           .replace(/\r/g,'')
           .replace(/\x01/g, '')
           .replace(/\[4z/g, '')

      var pkl
      if (Q.core.engine.__lastFGColour) { 
        pkl = '<span class="' + Q.core.engine.__lastFGColour
        if (Q.core.engine.__lastBGColour) {
          pkl += ' ' + Q.core.engine.__lastBGColour
        }
        pkl += '">' + s 
        s = pkl
      } else { s = '<span class="normal"> '+s }
      s += '</span><br>'
      
      // Matching lines
   
      if (line.match('<PROMPT>')) { 
        s = '<span class="mutrigger">&compfn;</span> ' + s
      } else if (Q.core.engine.__lineMatched) { 
        s = '<span class="triggered">&compfn;</span> ' + s 
      } else { 
        s = '<span class="untrigger">&compfn;</span> ' + s
      }
      Q.core.engine.__lineMatched = false

      // Timestamp
      var tstmp = currentTime.getTime()
      var mins  = currentTime.getMinutes()
      var secs  = currentTime.getSeconds()
      var mscs  = currentTime.getMilliseconds()
      tstmp = Q.core.utilities.lpad(mins,2,'0') + ':' + Q.core.utilities.lpad(secs,2,'0') + ':' + Q.core.utilities.lpad(mscs,3,'0') + ' '
      s = '<span class="timestamp mute">' + tstmp + '</span>' + s
      if (Q.core.settings.timestamp) { s = s.replace('class="timestamp mute"', 'class="timestamp mute" style="font-family: Lekton"') }

      // Modifying output
      if (Q.core.engine.__lineModify) {
        Q.core.engine.__lineModify = false
        while (Q.core.triggers.instructions.length) {
          var x = Q.core.triggers.instructions.shift()
              s = Q.core.engine.prettify(s,x)
        }
      }

      // Unescape HTML
      for (var i = 0; i < Q.core.engine.__htmlEscapes.length; i++) {
        var u = Q.core.engine.__htmlEscapes[i]
        s.replace(u.regex, u.character)
      }
      
      s = '<div class="QO-line">' + s + '</div>'
      // jm.print(s)
      Q.core.engine.__lastpacket = s
   
      // Safety catches
      Q.core.engine.__lineModify = false
      if (line.match('<PROMPT>')) { Q.core.triggers.instructions = [] }
      Q.core.engine.__Buffer++
      Q.core.vars.lines++
    }
    // for lastpackets
    if ( Q.core.engine.__orphanMatch.length > 0) { /* Observe */ } else { 
      outgoing.push(s)
    }
  }
  // var nxp = new Date(); console.log(nxp - pxn)
  Q.core.print(outgoing.join(''))
  // manage lastpackets for wrap
  if (Q.core.engine.__lastpackets.length > Q.core.settings.packetWrap) { Q.core.engine.__lastpackets.shift() }
  Q.core.engine.__lastpackets.push(outgoing)
}

Q.core.engine.handleTriggers = function(line) {
  // Strip tags
  Q.core.engine.__rawLine  = line
  line = Q.core.engine.simplifyLine(line)
  if (line.match('<PROMPT>')) {
    Q.core.engine.__lineMatched = false;
    $('body').trigger('prompt', line)
  } else {
    Q.core.triggers.discover(line)
  }
  return line
}

Q.core.engine.prettify = function(s,x) {
  var s = s || ''
  if (x.fx) {
    switch (true) {
      case x.fx == 'replace':
        s = s.replace(x.pattern,x.args[0])
        break
      case x.fx == 'multireplace':
        // assuming pattern is regex
        var mm = s.match(x.regex)
        if (mm) { for (var i=0; i<mm.length; i++) {
          var m = mm[i]
              m = m.replace(x.regex, x.newpattern)
              s = s.replace(mm[i], m)
        } }
        break
      case x.fx == 'whole-sub':
        if (x.options && x.options.line) {
          s = x.options.line
          s = '<span class="triggered">&compfn;</span> ' + s 
        }
        break
      case x.fx == 'delete':
        if (x.options && x.options.replace) {
          var regx = x.options.regx || null
          if (typeof regx != null) { s = s.replace( regx, x.options.replace ) } else {
            s = s.replace(x.options.pattern, x.options.replace) }
        } else {
          s = ''
        }
        break
      case x.fx == 'internal':
        if (x.options && x.options.what) {
          if (typeof x.options.regx != 'undefined') { s = s.replace( regx, x.options.so ) } else {
            s = s.replace(x.options.what, x.options.so) }
        }
        break
      case x.fx == 'append':
        s = s.replace('<br>', x.args[0] + '<br>')
      default:
        break
    }
  }
  return s
}

Q.core.engine.negotiateGMCPIronRealms = function() {
  if (Q.core.engine.__handshake >= 2) { return }
  console.log('(gmcp) Negotiating GMCP protocol for Iron Realms Entertainment.')
  Q.core.engine.__handshake = 1
  var instructions = [
    'IAC-DO-GMCP',
    'IAC-SB-GMCP-Core.Hello { "client": "jyve", "version": "1" }-IAC-SE',
    'IAC-SB-GMCP-Char.Items.Inv-IAC-SE',
    'IAC-SB-GMCP-IRE.Rift.Request-IAC-SE',
    'IAC-SB-GMCP-Core.Supports.Set [ "Char 1", "Char.Skills 1", "Char.Items 1", "Ping" ]-IAC-SE',
    'IAC-SB-GMCP-Core.Supports.Add [ "Comm.Channel 1", "IRE.Rift 1", "Room 1", "IRE.Display 1" ]-IAC-SE',
  ]
  for (var i = 0; i < instructions.length; i++) {
    var instruction = instructions[i]
    Q.core.Uint8(Q.core.engine.adonize(instruction))
  }
}

Q.core.engine.negotiateGMCPGenesis = function() {
  if (Q.core.engine.__handshake >= 1) { return }
  console.log('(gmcp) Negotiating GMCP protocol for Genesis Mud.')
  Q.core.engine.__handshake = 1
  var instructions = [
    'IAC-DO-GMCP',
    'IAC-SB-GMCP-Core.Hello { "client": "QueryOne", "version": "1" }-IAC-SE',
    'IAC-SB-GMCP-Core.Supports.Set [ "Char 1", "Room 1", "Comm 1" ]-IAC-SE',
    'IAC-SB-GMCP-Core.Token ""-IAC-SE',
    'IAC-SB-GMCP-Char.Vitals.Get ["health","fatigue","mana"]-IAC-SE',
  ]
  for (var i = 0; i < instructions.length; i++) {
    var instruction = instructions[i]
    Q.core.Uint8(Q.core.engine.adonize(instruction))
  }
}