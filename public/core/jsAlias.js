/*
  Sets:
    matches, but NOT Q.core.triggers.matches
 */
 
Q                = typeof Q               !== 'undefined' ? Q               : {}
Q.core           = typeof Q.core          !== 'undefined' ? Q.core          : {}

Q.aliases        = [] // !important, remake each time the file is reloaded

Q.core.aliases   = typeof Q.core.aliases  !== 'undefined' ? Q.core.aliases  : {}

Q.core.aliases.batch = function(list) {
  for (var i = 0; i < list.length; i++) {
    var m = list[i]
    for (var j = 0; j < m.patterns.length; j++) {
      Q.core.aliases.make(m.patterns[j], m.code, m.name)
    }
  }
}

Q.core.aliases.make = function(pattern, output, name, uuid) {
 var RE     = new RegExp(pattern)
 var name   = name || 'unnamed'
 // var parent = ''; if (typeof Q.git != 'undefined') { parent = Q.git.__parent }
 var uuid   = uuid || Q.core.utilities.uuid()
 Q.aliases.push({ pattern: pattern, output: output, RE: RE, name: name, uuid: uuid })
}

Q.core.aliases.eval = function(str) {
  var out  = []
  var temp = str.split('+alias')
  temp.shift()
  for (var i = 0; i < temp.length; i++) {
    var name     = ''
    var patterns = []
    var code     = ''
    var codeM    = temp[i].match(/\+do[\s\S]*?\-en/gim)
    if (codeM) { for (var j = 0; j < codeM.length; j++) { code = codeM[j].replace('+do','').replace('-en','') } }
    var nameM    = temp[i].match(/\+n[\s\S]*?\r?\n/g)
    if (nameM) { for (var j = 0; j < nameM.length; j++) { name = nameM[j].replace('+n ','').replace('\n','') } }
    var pattM    = temp[i].match(/\+p[\s\S]*?\r?\n/g)
    if (pattM) { for (var j = 0; j < pattM.length; j++) { patterns.push(pattM[j].replace('\n','').replace('+p ','')) } }
    out.push({name: name, patterns: patterns, code: code })
  }
  return out
}

Q.core.aliases.process = function(s) {
  var bool = false
  for (var i = 0; i < Q.aliases.length; i++) {
    var RE = Q.aliases[i].RE || {}
    if (typeof RE.exec != 'undefined' && RE.exec(s)) {
      bool = true
      matches = RE.exec(s)
      try { eval(Q.aliases[i].output) } catch(err) { console.log(err) }
    }
  }
  return bool
}

Q.core.aliases.make(
/^`js[ ]+(.*)$/,
`
 var s = matches[1]
 var x = eval(s)
 display(x)
 console.log(x)
`,
'Javascript executed')