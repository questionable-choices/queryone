Q               = typeof Q               !== 'undefined' ? Q               : {}
Q.core          = typeof Q.core          !== 'undefined' ? Q.core          : {}

Q.core.settings = typeof Q.core.settings !== 'undefined' ? Q.core.settings : {}

Q.core.settings.addressList = [

  { display     : 'Achaea', 
     address    : 'achaea.com/socket/',
     port       :  23,
     subprotocol: 'binary', 
     binaryType : 'arraybuffer',
     formalClose: true,
     website    : 'achaea.com', },

  { display     : 'Legend Mud', 
     address    : 'mud.legendmud.org',
     port       :  9999,
     subprotocol: 'binary', 
     binaryType : 'arraybuffer',
     formalClose: false,
     website    : 'fake.com', },
     
  { display     : 'MUME',
     address    : 'test.waba.be:443/mume/play/websocket',
     port       : 443,
     subprotocol: true,
     binaryType : 'arraybuffer',
     formalClose: false,
     website    : 'mume.org', },
     
  { display     : 'Genesis Mud', 
     address    : 'www.genesismud.org/websocket',
     port       :  23,
     subprotocol: false, 
     binaryType : 'arraybuffer',
     formalClose: false,
     website    : 'www.genesismud.org', },

  { display     : 'Imperian', 
     address    : 'imperian.com/socket/',
     port       :  23,
     subprotocol: 'binary', 
     binaryType : 'arraybuffer',
     formalClose: true,
     website    : 'imperian.com', },

  { display     : 'Starmourn', 
     address    : 'starmourn.com/socket/',
     port       :  23,
     subprotocol: 'binary', 
     binaryType : 'arraybuffer',
     formalClose: true,
     website    : 'starmourn.com', },
     
  // {address: 'aardmud.org/socket/',   port: 4010, display: 'Aardwolf', }, // no SSL :-(
  // {address: '216.251.47.10',         port: 4040, display: 'Realms of Despair', }, // no SSL, base64
  // {address: 'discworld.starturtle.net' port: 4243, display: 'Discworld', }, // not SSL as well
  
]
Q.core.settings.providerList = [
  'GitLab',
  'Github',
]

Q.core.settings.addressDefault  = Q.core.settings.addressList[0]
Q.core.settings.echoInput       = false
Q.core.settings.outputLimit     = 1200
Q.core.settings.packetWrap      = 4
Q.core.settings.timestamp       = true
Q.core.settings.wrapLimit       = 3

Q.core.settings.GSRSequenceDesc = 'GSR.Sequence'
Q.core.settings.userScripts     = [
  './user/modules.js'
]

Q.core.settings.commandLimit    = 10
Q.core.settings.ironrealms      = [
  'achaea.com/socket/',
  'imperian.com/socket/',
  'starmourn.com/socket/',
  'aetolia.com/socket/',
  'lusternia.com/socket/',
]