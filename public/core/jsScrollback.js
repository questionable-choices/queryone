Q      = typeof Q      !== 'undefined' ? Q      : {}
Q.core = typeof Q.core !== 'undefined' ? Q.core : {}

Q.core.scrollbackHandler = function() {
  var e = 'output'
  var a = '#addenda' // jquery
  var d = document.getElementById(e).scrollHeight
  var p = document.getElementById(e).scrollTop
  var h = document.getElementById(e).clientHeight
  var o = d - (p + h)
  
  if (o < Q.core.vars.scrollbackOffset) {
    // too close to bottom
    Q.core.vars.scrollback = false
    if (!$(a).hasClass('hidden')) { $(a).addClass('hidden'); $(a).empty() }  
  } else {
    // divert new input
    Q.core.vars.scrollback = true
    if ($(a).hasClass('hidden')) { $(a).removeClass('hidden') }  
  }
}

Q.core.doubleclickHandler = function() {
  fastdom.measure( function() {
    var h = document.getElementById('output').scrollHeight
    fastdom.mutate( function() {
      $('#output').scrollTop(h)
    })
  })
  $('#addenda').addClass('hidden')
  $('#addenda').empty()
  Q.core.vars.scrollback = false
  var input = $('#input')
  input.focus()
}