Q = typeof Q !== 'undefined' ? Q : {}

Q.core          = typeof Q.core          !== 'undefined' ? Q.core          : {}
Q.core.triggers = typeof Q.core.triggers !== 'undefined' ? Q.core.triggers : {}
Q.core.aliases  = typeof Q.core.aliases  !== 'undefined' ? Q.core.aliases  : {}

Q.objects       = typeof Q.objects       != 'undefined'  ? Q.objects       : []

Q.git = typeof Q.git !== 'undefined' ? Q.git : {}

// External Functions
Q.git.parseScripts   = Q.core.parseScripts   // ! Dependent on load sequence, decided to define it down below
Q.git.batchTriggers  = Q.core.triggers.batch
Q.git.evalTriggers   = Q.core.triggers.eval
Q.git.batchAliases   = Q.core.aliases.batch
Q.git.evalAliases    = Q.core.aliases.eval

// Settings
Q.git._provider  = 'Github'
Q.git._url       = 'https://gitlab.com/api/v4/snippets'

Q.git._urlGitlab = 'https://gitlab.com/api/v4/snippets'
Q.git._urlGithub = 'https://api.github.com/gists?access_token='

Q.git._sequence  = 'QO Sequence'
Q.git._taxonomy  = 'QO: '
Q.git._token     = '7b6b1e1dc7a49bd5b7a92fc14c18978f142a4d3d' // 'F2DBNkV8vKtcjS_ba8Xe'
Q.git._prefix    = '<span class="mute"> > gsr:</span><span class="normal"> ' 

Q.git._tertiary  = true

// Variables for Exposure
Q.git.raw            = Q.git.raw            || []

// Internal Variables
Q.git.__token        = Q.git.__token        || ''
Q.git.__snippets     = Q.git.__snippets     || []
Q.git.__sequences    = Q.git.__sequences    || []
Q.git.__forRetrieval = Q.git.__forRetrieval || []
Q.git.__parent       = Q.git.__parent       || null
Q.git.__files        = Q.git.__files        || []

Q.git.registry       = Q.git.registry       || {}

// Setters & Getters
Q.git.setToken = function(token) { Q.git.__token = token }
Q.git.setList  = function(list)  { Q.git.__snippets = clone(list) }

Q.git.report = function(msg, fin) {
  var s = Q.git._prefix + msg
  if (fin) { s += '<br >' }
  Q.core.print(s)
  if ($('#output').length) { $('#output').scrollTop(document.getElementById('output').scrollHeight) }
}


Q.git.begin = function() {
  $.ajax({
   url       : 'https://api.github.com/gists',
   beforeSend: function(xhr) { xhr.setRequestHeader('Authorization', 'token 7b6b1e1dc7a49bd5b7a92fc14c18978f142a4d3d') },
   success   : function(e) { $('body').trigger('QO-GitService-FirstRequest', {data: e, host: 'Github'}) },
  })
  /*
  $.ajax({
   url       : 'https://gitlab.com/api/v4/snippets',
   beforeSend: function(xhr) { xhr.setRequestHeader('Private-Token', 'F2DBNkV8vKtcjS_ba8Xe') },
   success   : function(e) { $('body').trigger('QO-GitService-FirstRequest', {data: e, host: 'GitLab'}) },
  })*/
}

Q.git.process = function(e,v) {
  var data = e.data
  var host = e.host
  console.log(host)
  console.log(data)

  var character  = Q.core.vars.player
  var game       = Q.core.vars.game
  var identifier = 'QueryOne'

  // All Hosted files are either a Sequencer or a Scripted
  //   First we locate all Files that might be relevant to this Player in this Game
  //   Within these Files, we separate them into Sequencers and Scripteds
  //   There technically should only be one valid Sequencer for each Player in each Game
  //   We find one valid Sequencer and reject the others
  //   We then retrieve all Scripteds that are relevant
  //   We sort these based on our Hierarchy
  //   Then with the Sequencer, we sort within the Hierarchy   
  
  // Sequencers:
  //   QueryOne.GitService.Achaea.Tysandr.qs
  //   QueryOne.GitService.Achaea.qs
  //   QueryOne.GitService.Tysandr.qs
  //   QueryOne.GitService.qs
  
  // Files:
  //   Achaea.Tysandr.module.qs
  //   Achaea.QueryOne.module.qs
  //   Tysandr.module.qs
  //   QueryOne.module.qs

  var p = $.when(1)
  // Flatten Github first
  var reverse = []
  if (host == 'Github') {
    for (var i = 0; i < data.length; i++) {
      var f      = data[i].files
      for (var name in f) {
        f[name].parent      = data[i].id
        f[name].description = data[i].description
        f[name].id          = Q.core.utilities.uuid()
        reverse.push(f[name])
      }
    }
    // console.log(reverse)
    data = reverse
  }
  
  for (var i = 0; i < data.length; i++) {
    let item = data[i]
    // console.log(item.description); console.log(game + ' ' + character + ' ' + identifier)
    if (game && item.description.match(game) || character && item.description.match(character) || identifier && item.description.match(identifier)) {
      if (host == 'GitLab') {
        let url  = ''
        let uuid = item.id
        url = 'https://gitlab.com/api/v4/snippets' + '/' + item.id + '/raw'
        Q.git.raw.push({
          __host     : host, 
          __parent   : '',
          __parentURL: url,
          __desc     : item.description,
          __uuid     : item.id,
          __fileName : item.file_name,
          __url      : url,
        })
        p = p.then(function() {
          return $.ajax({ url: url, beforeSend: function(xhr){xhr.setRequestHeader('Private-Token', 'F2DBNkV8vKtcjS_ba8Xe')}, })
        }).then(function(data) {
          for (var j = Q.git.raw.length - 1; j > -1; j--) {
            if (Q.git.raw[j].__uuid == uuid) {
              Q.git.raw[j].data = Q.core.utilities.clone(data)
            }
          }
        })
      } else if (host == 'Github') {
        let url  = item.raw_url
        let uuid = item.id
        Q.git.raw.push({
          __host     : host,
          __parent   : item.parent,
          __parentURL: item.url,
          __desc     : item.description,
          __uuid     : item.id,
          __fileName : item.filename,
          __url      : item.raw_url,
        })
        p = p.then(function() {
          return $.ajax({ url: url })
        }).then(function(data) {
          for (var j = Q.git.raw.length - 1; j > -1; j--) {
            if (Q.git.raw[j].__uuid == uuid) {
              Q.git.raw[j].data = Q.core.utilities.clone(data)
            }
          }
        })
      }
    }
  }
  p = p.then(function() {
    // Sort here?
  
    console.log(Q.git.raw)
    $('body').trigger('QO-GitService-FirstRead')
  })

}

Q.git.readFiles = function() {
  
  var m = Q.git.raw

  for (var i = 0; i < m.length; i++) {
    var raw = m[i]
    raw.specified = []
    var r = Q.git.splitData(raw.data, raw.__uuid)
    m[i].__parsed = r

    // Flatten this
    for (var j = 0; j < r.scripts.length; j++) {
      var e = r.scripts[j]
          e.__gitOrigin = raw.__uuid
      Q.git.addElement(e.uuid, e, true)
    }
    for (var j = 0; j < r.triggers.length; j++) {
      var e = r.triggers[j]
          e.__gitOrigin = raw.__uuid
      Q.git.addElement(e.uuid, e, true)
    }
    for (var j = 0; j < r.aliases.length; j++) {
      var e = r.aliases[j]
          e.__gitOrigin = raw.__uuid
      Q.git.addElement(e.uuid, e, true)
    }
  }
  
  $('body').trigger('QO-GitService-ScriptsReady')
  
}

Q.git.splitData = function(data, origin) {
  var scriptRegex   = /\+script\+([\s\S]*?)\-script\-/g
  var triggersRegex = /\+triggers\+([\s\S]*?)\-triggers\-/g
  var aliasesRegex  = /\+aliases\+([\s\S]*?)\-aliases\-/g

  var scripts  = []
  var triggers = []
  var aliases  = []

  var header = ''
  var pid    = ''
  if (data.match(/^identify .+/)) {
    header = data.match(/^identify .+/)[0]
    header = header.replace('identify ','')
    for (var i = 0; i < Q.objects.length; i++) {
      var item = Q.objects[i]
      if (item.type == 'module' && item.name == header) {
        pid = item.uuid
        break
      }
    }
    if (pid == '') {
      pid = Q.core.utilities.uuid()
      Q.git.addElement(pid, {type: 'module', order: 'reverse', origin: origin, name: header})
    }
  }

  var temp = data.match(scriptRegex)
  if (temp) { for (var i = 0; i < temp.length; i++) {
    var script = {}
    var u = Q.core.utilities.uuid()
    var H = /\+script\+ \+n (.+)/
    var m = temp[i]
    var n = ''
    if (m.match( H )) {
      n = m.match( H )
      n = n[1] || ''
    }
    m = m.replace('-script-','').replace( H ,'')
    script.script = m
    script.name = n
    script.uuid = u
    script.type = 'script'
    script.parent = pid
    scripts.push(script)
  }}

  temp = data.match(triggersRegex)
  if (temp) { for (var i = 0; i < temp.length; i++) {
    var m = temp[i]
    m = m.replace('+triggers+','').replace('-triggers-','')
    m = m.split('+trigger')
    m.shift()
    for (var j = 0; j < m.length; j++) {
      var temp     = m[j]
      var name     = ''
      var patterns = []
      var code     = ''
      var codeM    = temp.match(/\+do[\s\S]*?\-en/gim)
      if (codeM) { for (var k = 0; k < codeM.length; k++) { code = codeM[k].replace('+do','').replace('-en','') } }
      var nameM    = temp.match(/\+n[\s\S]*?\r?\n/g)
      if (nameM) { for (var k = 0; k < nameM.length; k++) { name = nameM[k].replace('+n ','').replace('\n','') } }
      var pattM    = temp.match(/\+p[\s\S]*?\r?\n/g)
      if (pattM) { 
        for (var k = 0; k < pattM.length; k++) { 
          patterns.push({pattern: pattM[k].replace('\n','').replace('+p ',''), uuid: Q.core.utilities.uuid()}) 
        } 
      }
      triggers.push({
         name: name, 
         patterns: patterns, 
         response: code, 
         uuid: Q.core.utilities.uuid(), 
         type: 'trigger', 
         parent: pid 
      })
    }
  }}  

  temp = data.match(aliasesRegex)
  if (temp) { for (var i = 0; i < temp.length; i++) {
    var m = temp[i]
    m = m.replace('+aliases+','').replace('-aliases-','')
    m = m.split('+alias+')
    m.shift()
    for (var j = 0; j < m.length; j++) {
      var temp     = m[j]
      var name     = ''
      var patterns = []
      var code     = ''
      var codeM    = temp.match(/\+do[\s\S]*?\-en/gim)
      if (codeM) { for (var k = 0; k < codeM.length; k++) { code = codeM[k].replace('+do','').replace('-en','') } }
      var nameM    = temp.match(/\+n[\s\S]*?\r?\n/g)
      if (nameM) { for (var k = 0; k < nameM.length; k++) { name = nameM[k].replace('+n ','').replace('\n','') } }
      var pattM    = temp.match(/\+p[\s\S]*?\r?\n/g)
      if (pattM) { 
        for (var k = 0; k < pattM.length; k++) { 
          patterns.push({pattern: pattM[k].replace('\n','').replace('+p ',''), uuid: Q.core.utilities.uuid()}) 
        } 
      }
      aliases.push({
         name: name, 
         patterns: patterns, 
         output: code, 
         uuid: Q.core.utilities.uuid(), 
         type: 'alias', 
         parent: pid 
      })
    }
  }}

  var out = {scripts: scripts, triggers: triggers, aliases: aliases}
  console.log(out)

  return out
}


Q.git.addElement = function(uuid, options, passThrough) {
  var options = options || {}
  var item = {}
  if (!passThrough && options.uuid) {
    console.log('Item #' + options.uuid + ' already exists.')
  } else {
      item.uuid     = uuid
      item.file     = options.file     || ''
      item.parent   = options.parent   || ''
      item.name     = options.name     || ''
      item.type     = options.type     || 'script'
      item.pattern  = options.pattern  || ''
      item.patterns = options.patterns || []
      item.response = options.response || ''  // triggers
      item.output   = options.output   || ''  // aliases
      item.script   = options.script   || ''
      item.__gitOrigin = options.__gitOrigin //  || options.origin

      item.response = item.response.trim()
      item.output   = item.output.trim()
      item.script   = item.script.trim()
  if (typeof options.parent == 'undefined') {
    item.parent = $('#' + uuid).parent().attr('id')
    if (typeof item.parent == 'undefined') {
      item.parent = 'scripting-navigator-content'
    }
  }
  if (options && options.order == 'reverse') {
    Q.objects.unshift(item)
  } else {
    Q.objects.push(item)
  }
  }
}

Q.git.build = function() {
  for (var i = 0; i < Q.objects.length; i++) {
    var item = Q.objects[i]
    if (item.type == 'script') {
      // console.log(item)
      try {
        eval(item.script)
      } catch(err) { 
        console.log(err)
        console.log(item.uuid) 
        console.log(item.script)
      }
    }
    if (item.type == 'trigger') {
      for (var j = 0; j < item.patterns.length; j++) {
        Q.core.triggers.make(item.patterns[j].pattern, item.response, item.name, item.patterns[j].uuid)
      }
    }
    if (item.type == 'alias') {
      for (var j = 0; j < item.patterns.length; j++) {
        Q.core.aliases.make(item.patterns[j].pattern, item.output, item.name, item.patterns[j].uuid)
      }
    }
  }
}

Q.git.cloudify = function() {
  var str = ''

  var newmodules = {}
  var origins    = {}
  /*
  for (var i = 0; i < Q.objects.length; i++) {
    var item = Q.objects[i]
    if (item.type == 'module') {
      if (typeof item.__gitOrigin == 'undefined') {
        // Collect all your children
        newmodules[item.uuid] = []
      } else {
        origins[item.uuid] = []
      }
    }
  }*/

  for (var i = 0; i < Q.objects.length; i++) {
    var item = Q.objects[i]
    if (typeof item.__gitOrigin == 'undefined') {

    } else {
      origins[item.__gitOrigin] = origins[item.__gitOrigin] || []
      origins[item.__gitOrigin].push(item.uuid)
    }
  }
 
  for (var gitOrigin in origins) {
    var output   = ''
    var aliases  = ''
    var triggers = ''
    var parent   = ''
    for (var i = 0; i < origins[gitOrigin].length; i++) {
      var uuid = origins[gitOrigin][i]
      let item = Q.git.retrieve(uuid)
      parent = item.__gitOrigin // The presumption here is all these items share the same __gitOrigin, going to use the last one !important
      if (item.type == 'script') {
        output += '+script+ +n ' + item.name + '\n'
        output += item.script + '\n'
        output += '-script-'
      }
      if (item.type == 'alias') {
        aliases += '+alias+\n'
        aliases += ' +n ' + item.name + '\n'
        for (var j = 0; j < item.patterns.length; j++) {
          aliases += ' +p ' + item.patterns[j].pattern + '\n'
        }
        aliases += ' +do\n'
        aliases += Q.git.package(item.output) // + '\n'
        aliases += ' -en\n'
      }
      if (item.type == 'trigger') {
        triggers += '+trigger\n'
        triggers += ' +n ' + item.name + '\n'
        for (var j = 0; j < item.patterns.length; j++) {
          triggers += ' +p ' + item.patterns[j].pattern + '\n'
        }
        triggers += ' +do\n'
        triggers += Q.git.package(item.response) // + '\n'
        triggers += ' -en\n'
      }
    }
    output += '\n\n'
    if (aliases != '')  { aliases  = '+aliases+\n'  + aliases  + '-aliases-\n\n'; output += aliases;  }
    if (triggers != '') { triggers = '+triggers+\n' + triggers + '-triggers-\n';  output += triggers; }
    Q.git.updateGist(parent, output)
  }
}

Q.git.updateGist = function(uuid, output) {
  for (var i = 0; i < Q.git.raw.length; i++) {
    var rawFile = Q.git.raw[i]
    if (rawFile.__uuid == uuid) {
      console.log('Going to update ' + uuid + '.')
      console.log('Update is: \n' + output )
      var f = {}
          f[rawFile.__fileName] = {content: output}
      var data = {
       "description": rawFile.__desc,
      }
      data['files'] = f
      data = JSON.stringify(data)
      $.ajax({
        url       : rawFile.__parentURL,
        type      : 'PATCH',
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', 'token 7b6b1e1dc7a49bd5b7a92fc14c18978f142a4d3d')
        },
        data      : data,
      }).done(function(r) {
        console.log(r)
      })

    }
  }
}


Q.git.package = function(string) {
  var pre = /^    /
  var o = string.split('\n')
  for (var i = 0; i < o.length; i++) {
    o[i] = o[i].replace(pre,'') + '\n'
    o[i] = '    ' + o[i]
  }
  return o.join('')
}

Q.git.retrieve = function(uuid) {
  for (let i = 0; i < Q.objects.length; i++) {
    if (Q.objects[i].uuid == uuid) {
      return Q.objects[i]
    }
  }
  return false
}

Q.git.getAllChildren = function(uuid) {
  var children = []
  for (var i = 0; i < Q.objects.length; i++) {
    var item = Q.objects[i]
    if (item.parent == uuid) {
      children.push(item.uuid)
    }
  }
  return children
}