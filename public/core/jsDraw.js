Q                = typeof Q               !== 'undefined' ? Q               : {}
Q.core           = typeof Q.core          !== 'undefined' ? Q.core          : {}

// UI Function
Q.core.draw = function() {
  $('#panel-options').remove()
  $('#panel-connections').remove()
  var addresses = Q.core.settings.addressList
  var providers = Q.core.settings.providerList
    
  var d  = ''
      d += '<div id="panel-options">'
      d +=   '<div id="options-list">'
      d +=     '<div id="options-provider"></div>'
      d +=     '<div id="options-privateToken"><input id="options-privateToken-input" placeholder="  Private Token here"></input></div>'
      d +=   '</div>'
      d += '</div>'
      d += '<div id="panel-connections">'
      d +=   '<div id="connections-list">'
      d +=   '<div id="connections-address-selector">'
      d +=     '<div id="connections-addresses"></div>'
      d +=     '<div id="connections-address-custom"><input id="connections-address-input" placeholder="Custom address here"></input></div>'
      d +=   '</div></div>'
      d += '</div>'
      d += '<div id="panel-scripts" onclick="Q.core.scriptingToggle()"><div class="display">{}</div></div>'
      d += '<div id="QO-connection-light" class="closed">'
      d +=   '<div id="QO-connection-light-internal"></div>'
      d += '</div>'
  $('#panel').append(d)
  
  d = ''
  for (var i = 0; i < providers.length; i++) {
    d += '<div id="options-provider-' + providers[i] + '" class="options-item" onclick="Q.core.selectProvider(\'' + providers[i] + '\')">  ' + providers[i] + '</div>'
  }
  $('#options-provider').append(d)
  
  d = ''
  for (var i = 0; i < addresses.length; i++) {
    var t = addresses[i]
    d += '<div id="connections-address-' + t.display.replace(/\s/g,'-') + '" class="connections-item" onclick="Q.core.selectAddress(\'' + t.display + '\')">  ' + t.display + '</div>'
  }
  $('#connections-addresses').append(d)
  
  // Prefill Token input with git._token
  $('#options-privateToken input').val(Q.git._token)
  
  // Trigger event when token input changes
  $('#options-privateToken input').change(function() {
    $('body').trigger('QO-token-changed')
  })
}

