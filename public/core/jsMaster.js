Q                = typeof Q               !== 'undefined' ? Q               : {}
Q.core           = typeof Q.core          !== 'undefined' ? Q.core          : {}

Q.core.__input   = $('#input')
Q.core.__output  = $('#output')
Q.core.__addenda = $('#addenda')

// Core commands for the client
Q.core.commands  = typeof Q.core.commands !== 'undefined' ? Q.core.commands : {}

Q.core.commands.connectRequest    = new RegExp('^\-c$'   )
Q.core.commands.connectSpecified  = new RegExp('^\-c .*$')
Q.core.commands.reloadAll         = new RegExp('^\-ra$'  )
Q.core.commands.reloadSignal      = new RegExp('^\-r$'   )
Q.core.commands.userReload        = new RegExp('^\-ru$'  )
Q.core.commands.connectGitService = new RegExp('^\-g$'   )

// Main Functions
Q.core.redraw = function() {
  var F = parseFloat( window.getComputedStyle(document.getElementById('input'), null).getPropertyValue('font-size') )
  $('#input').css('height', F + 4)
  $('#output').css('height', window.innerHeight - $('#input').height() - 7)
  
  // Hanging Indent .hangingindent { padding-left: 22px ; text-indent: -22px ; }
  //   first calculate width of font
  var e = {}
  var fn = function(t, f) {
    e.element = document.createElement('canvas')
    e.context = e.element.getContext('2d')
    e.context.font = f
    var g = {width: e.context.measureText(t).width, height: parseInt(e.context.font)}
    return g
  }
  var m = fn('a', $('#output').css('font'))
  var w = m.width
      w = 12 * w + 'px'
  var s = $('<style>.QO-line { padding-left: ' + w + '; text-indent: -' + w + '; }</style>')
  $('html > head').append(s)
  // $('.QO-line').css('padding-left', w).css('text-indent', '-' + w)
}

Q.core.ready = function() {
  // Basic initiation housekeeping
  $('#panel').append('<div id="QO-performance"></div><div id="QO-ping"></div>')

  // Setting Provider
  Q.core.selectProvider(Q.git._provider)
  Q.core.setConnection(Q.core.settings.addressDefault)
    
  var input = Q.core.__input
      input.focus()
  // Input focus on clicking without selection
  $(document).on('click', function(e) {
    if (window.getSelection && window.getSelection().toString().length > 0) { 
      // do nothing
    } else if ($('input').is(':focus')) {
      // also do nothing
    } else if (typeof Q.codeAce != 'undefined' && Q.codeAce.isFocused()) {
      // also do nothing
    } else {
      input.focus()
    }
  })
  // Handle ENTER
  $(document).keypress(function(e) {
    if (e.which == 13) {
      if (typeof Q.codeAce != 'undefined' && Q.codeAce.isFocused()) { } else {
        e.preventDefault()
      
        Q.core.vars.performance = new Date().getTime()
        Q.core.vars.performanceFlag = true
      
        input.select()
        var str = input.val()
        Q.core.vars.commandHistory.unshift(str)
        if (Q.core.vars.commandHistory.length > Q.core.settings.commandLimit) { Q.core.vars.commandHistory.pop() }
        Q.core.vars.commandPosition = 0
        Q.core.input(str)
      }
    }
  })
  $(document).keydown(function(e) {
    if (e.which == 40) {
      Q.core.vars.commandPosition -= 1
      var s = ''
      if (Q.core.vars.commandPosition < 1) {
        Q.core.vars.commandPosition = 0
      } else {
        s = Q.core.vars.commandHistory[(Q.core.vars.commandPosition - 1)]
      }
      input.val(s).select()
    } else if (e.which == 38) {
      Q.core.vars.commandPosition += 1
      var s = ''
      if (Q.core.vars.commandPosition < (Q.core.vars.commandHistory.length + 1)) {
      } else { Q.core.vars.commandPosition = Q.core.vars.commandHistory.length /* Q.core.vars.commandPosition = 0 */ }
      s = Q.core.vars.commandHistory[(Q.core.vars.commandPosition - 1)]
      input.val(s).select()
    }
  })
  // Events
  $('body').on('QO-disconnect-and-new', function() {
    console.log('(QO): Cutting existing connection & reconnecting to ' + Q.core.vars.address + ':' + Q.core.vars.port + '.')
  })
  $('body').on('snippets-read', function() {
    console.log('(QO): Snippets read.')
  })
  $('body').on('QO-GMCP-Negotiation', function() {
    if (Q.core.engine.__handshake < 2) {
      console.log('(QO-Master) Received GMCP Negotiation Request.')
    }
    if (Q.core.w.url == 'wss://www.genesismud.org/websocket') {
      Q.core.engine.negotiateGMCPGenesis()
    }
    if (Q.core.settings.ironrealms.indexOf(Q.core.vars.address) != -1) {
      Q.core.engine.negotiateGMCPIronRealms()
    }
  })
  $('body').on('QO-token-changed', function() {
    Q.git._token = $('#options-privateToken input').val()
    Q.git.begin()
  })
  $('body').on('QO-performance', function(e, v) {
    if (v > 2) { document.getElementById('QO-performance').textContent = v + 'ms' }
  })
  $('body').on('script-moused', function() {
    $('#scripting-main').css('filter','opacity(100%)')
  })
  $('body').on('script-unmoused', function() {
    $('#scripting-main').css('filter','opacity(25%)')
  })

  $('body').on('QO-connecting', function() {
    $('#QO-connection-light')
       .removeClass('open')
       .removeClass('closing')
       .removeClass('closed')
       .addClass('connecting')
  })
  $('body').on('QO-connected', function() {
    $('#QO-connection-light')
       .removeClass('connecting')
       .removeClass('closing')
       .removeClass('closed')
       .addClass('open')
  })
  $('body').on('QO-disconnected', function() {
    $('#QO-connection-light')
       .removeClass('connecting')
       .removeClass('open')
       .removeClass('closing')
       .addClass('closed')
  })
  $('body').on('QO-GitService-FirstRequest', function(e, v) {
    Q.git.process(v)
  })
  $('body').on('QO-GitService-FirstRead', function(e, v) {
    Q.git.readFiles()
  })
  $('body').on('QO-GitService-ScriptRead', function(e, v) {
    console.log(v)
  })
  $('body').on('QO-GitService-ScriptsReady', function(e, v) {
    console.log('Building Q.objects... standby.')
    Q.git.build()
  })
}

Q.core.print = function(line) {
  if (Q.core.vars.scrollback) { Q.core.__addenda.append(line) }
  Q.core.__output.append(line)
}

Q.core.input = function(userInput) {
  switch (true) {
    case Q.core.commands.connectRequest.test(userInput):
      Q.core.connect()
      break
    case Q.core.commands.connectSpecified.test(userInput):
      Q.core.connect(userInput)
      break
    case Q.core.commands.reloadAll.test(userInput):
    case Q.core.commands.reloadSignal.test(userInput):
      Q.core.reload()
      break
    case Q.core.commands.userReload.test(userInput):
      Q.core.userReload()
      break
    case Q.core.commands.connectGitService.test(userInput):
      if (typeof Q.git !== 'undefined') {
        Q.git.begin()
      }
      break
    default:
      if (Q.core.settings.echoInput) { Q.core.print('<span class="echo">' + userInput + '</span><br>') }
      Q.core.parcel(userInput)
      break
  }
}

Q.core.parcel = function(input) {
  var x = Q.core.aliases.process(input)
  if (!x) { Q.core.send(input) }
}

Q.core.send = function(input) {
  input += '\r\n'
  var a = input.split('')
  for (var i = 0; i < a.length; i++) { 
    a[i] = a[i].charCodeAt(0)
  }
  a = new Uint8Array(a)
  // !Important
  if (typeof Q.core.w != 'undefined' && Q.core.w.readyState == 1) { Q.core.w.send(a) } else {
    Q.core.print('<span class="darkred">Not connected: </span><span class="normal">' + input + '</span>')
    if ($('#output').length) {
      $('#output').scrollTop(document.getElementById('output').scrollHeight) }
  }
}

Q.core.Uint8 = function(str) {
  var a = new Uint8Array(str)
  Q.core.w.send(a)
}

Q.core.selectProvider = function(provider) {
  if (typeof Q.git !== 'undefined') {
    Q.git._provider = provider
    $('.options-item').removeClass('active')
    $('#options-provider-' + provider).addClass('active')
  }
}

Q.core.selectAddress = function(game) {
  if (!game) { return }
  if (typeof Q.core.settings.addressList !== 'undefined') {
    $('.connections-item').removeClass('active')
    for (var i = 0; i < Q.core.settings.addressList.length; i++) {
      var t = Q.core.settings.addressList[i]
      if (t.display == game) {
        Q.core.setConnection(t) 
        $('#connections-address-' + game.replace(' ','-')).addClass('active')
        $('body').trigger('QO-disconnect-and-new')
        break
      }
    }
  }
}

Q.core.setConnection = function(connection) {
  Q.core.vars.address     = connection.address
  Q.core.vars.port        = connection.port
  Q.core.vars.binaryType  = connection.binaryType
  Q.core.vars.subprotocol = connection.subprotocol
  Q.core.vars.formalClose = connection.formalClose

  if (typeof Q.core.vars.port        == 'undefined') { Q.core.vars.port        = 23 }
  if (typeof Q.core.vars.binaryType  == 'undefined') { Q.core.vars.binaryType  = 'arraybuffer' }
  if (typeof Q.core.vars.subprotocol == 'undefined') { Q.core.vars.subprotocol = false }
  if (typeof Q.core.vars.formalClose == 'undefined') { Q.core.vars.formalClose = false }
}

Q.core.userReload = function() { // https://stackoverflow.com/a/44137377/6881999
  if (Q.core.vars.reloadList.length == 0) {
    Q.core.vars.reloadTime = new Date()
    Q.core.vars.reloadList = Q.core.utilities.clone(Q.core.settings.userScripts)
  }
  if (Q.core.vars.reloadCount >= Q.core.vars.reloadList.length) {
    Q.core.vars.reloadCount = 0
    Q.core.vars.reloadList  = []
    return
  }
  var address  = Q.core.vars.reloadList[Q.core.vars.reloadCount]
  var T        = document.createElement('script')
      T.src    = address
      T.onload = Q.core.userReloadCallback
  document.getElementsByTagName('head')[0].appendChild(T)
  Q.core.__output.append('<span class="mute"> > Loading <span class="normal><i>' + Q.core.utilities.rpad(address, 30) + '</i></span></span>')
}

Q.core.userReloadCallback = function() {
  Q.core.__output.append('<span class="mute"> success.</span>')
  Q.core.reloadCount++
  if (Q.core.reloadCount >= Q.reloadList.length) {
    var t = new Date() - Q.core.reloadTime
    Q.core.__output.append('<span class="mute"> ( <span class="link">' + t + '</span>ms)</span><br >')
    Q.core.__output.scrollTop(document.getElementById('output').scrollHeight) // !important
    if (typeof GitServiceReady  == 'function') { GitServiceReady()  }
    if (typeof LocalSystemReady == 'function') { LocalSystemReady() }
  }
  Q.core.__output.append('<br >')
  Q.core.userReload()
}

Q.core.timestampOn = function() {
  $('.timestamp').css('font-family','"Lekton"')
  if ($('#output').length) {
    $('#output').scrollTop(document.getElementById('output').scrollHeight) }
}

Q.core.timestampOff = function() {
  $('.timestamp').css('font-family','"AdobeBlank"')
  $('.timestamp').css('font-size','3pt')
  $('.timestamp').css('line-height','5px')
  $('.timestamp').css('white-space','pre-wrap')
  if ($('#output').length) {
    $('#output').scrollTop(document.getElementById('output').scrollHeight) }
}

Q.core.evalscript = function(s) { // !important
  var out = []
  var temp = s.split('+script')
  temp.shift()
  for (var i=0;i<temp.length;i++) {
    var t = temp[i]
    t = t.replace('-script','')
    try { eval(t) } catch(err) { log(err) }
  }
}

Q.core.timestamp = function(str) {
  var lpad = Q.core.utilities.lpad
  var s = ''
  var str = str || ''
  if (str.match('<PROMPT>')) {
    s = '<span class="mutrigger">&compfn;</span> ' + s
  } else {
    s = '<span class="sytrigger">&compfn;</span> ' + s
  }
  var t = new Date()
  var m = t.getMinutes()
  var n = t.getSeconds()
  var o = t.getMilliseconds()
      t = lpad(m,2,'0') + ':' + lpad(n,2,'0') + ':' + lpad(o,3,'0') + ' '
      s = '<span class="timestamp mute">' + t + '</span>' + s
 if (Q.core.settings.timestamp) { s = s.replace('class="timestamp mute"', 'class="timestamp mute" style="font-family: Lekton"') }
 
 return s
}