Q      = typeof Q      !== 'undefined' ? Q      : {}
Q.core = typeof Q.core !== 'undefined' ? Q.core : {}

Q.core.CSSList = [
  'core/csMain.css',
  'core/csColors.css',
  'core/csPanel.css',
  'core/csScripting.css',
]

Q.core.CSSLoad = function(href) {
  var css = $('<link>', {
    'rel'   : 'stylesheet',
    'type'  : 'text/css',
    'href'  : href + '?' + Math.random(),
  })[0]
  document.getElementsByTagName('head')[0].appendChild(css)
}

Q.core.CSSReload = function() {
  for (var i = (Q.core.CSSList.length - 1); i > -1; i--) {
    Q.core.CSSLoad(Q.core.CSSList[i])
  }
}

Q.core.CSSReload()