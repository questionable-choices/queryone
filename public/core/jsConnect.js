/*
  Requires:
    Q.core.vars.address
 */

Q             = typeof Q             !== 'undefined' ? Q             : {}
Q.core        = typeof Q.core        !== 'undefined' ? Q.core        : {}
Q.core.vars   = typeof Q.core.vars   !== 'undefined' ? Q.core.vars   : {}
Q.core.engine = typeof Q.core.engine !== 'undefined' ? Q.core.engine : {}

Q.core.connect = function(addr, port) {
  var address = Q.core.vars.address
  if (addr) { address = addr }
  Q.core.w = new WebSocket('wss://' + address, 'binary')
  Q.core.w.binaryType = 'arraybuffer'
  Q.core.w.onopen     = Q.core.open
  Q.core.w.onmessage  = Q.core.engine.receive
  Q.core.w.onclose    = Q.core.close
}

Q.core.open = function(e) {
  Q.core.vars.address = Q.core.w.url.replace('wss://','')
  console.log('Connected to wss://' + Q.core.vars.address + ' successfully.')
  Q.core.vars.sessionTime = new Date()
  Q.core.vars.connected   = true
  $('body').trigger('connected')
}

Q.core.close = function(e) {
  var msg  = 'Disconnected from server: wss://' + Q.core.vars.address
  console.log(msg + '.')
  Q.core.print(' <span class=">' + msg + '</span><br >')
  
  Q.core.vars.connected     = false
  Q.core.engine.__handshake = 0
  
  var d = Q.core.utilities.interval(Q.core.vars.sessionTime, new Date())
  var s = ''
  s += d.days + ' days '
  s += d.hrs  + ' hrs '
  s += d.mins + ' mins '
  s += d.secs + ' secs '
  s += d.msecs+ ' msecs'
  Q.core.print('   <span class="sysecho">Session duration : ' + s                   + '</span><br>')
  Q.core.print('   <span class="sysecho">Packets processed: ' + Q.core.vars.packets + '</span><br>')
  Q.core.print('   <span class="sysecho">Lines processed  : ' + Q.core.vars.lines   + '</span><br>')
  Q.core.vars.packets = 0
  Q.core.vars.lines   = 0
  $('#output').scrollTop(document.getElementById('output').scrollHeight) // !important
  $('body').trigger('disconnected')
}