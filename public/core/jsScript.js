Q       = typeof Q       !== 'undefined' ? Q      : {}
Q.core  = typeof Q.core  !== 'undefined' ? Q.core : {}

Q.scripts = []

Q.core.parseScripts = function(script) {
  // var out = []
  var t   = script.split('+script')
      t.shift()
  for (var i = 0; i < t.length; i++) {
    var s = t[i]
    var n = s.match(/\+n .+?\n/)
    if (n) { n = n[0].replace('+n ','') }
        s = s.replace(/\+n .+?\n/, '')
        s = s.replace('-script','')
    try { k = eval(s) } 
    catch(error) {
      console.log(error)
      k = 'Error: incomplete load.'
      $('#output').append('<span class="darkred"> error</span>')
    }
    var uuid = Q.core.utilities.uuid()
    var parent = ''; if (typeof Q.git != 'undefined') { parent = Q.git.__parent }
    Q.scripts.push({uuid: uuid, outcome: k, name: n, script: script, parent: parent})
  }
  // return out
}