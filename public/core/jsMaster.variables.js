Q               = typeof Q               !== 'undefined' ? Q               : {}
Q.core          = typeof Q.core          !== 'undefined' ? Q.core          : {}
Q.core.aliases  = typeof Q.core.aliases  !== 'undefined' ? Q.core.aliases  : {}
Q.core.triggers = typeof Q.core.triggers !== 'undefined' ? Q.core.triggers : {}

Q.core.vars     = typeof Q.core.vars     !== 'undefined' ? Q.core.vars     : {}

Q.core.vars.address     = typeof Q.core.vars.address     !== 'undefined' ? Q.core.vars.address     : ''
Q.core.vars.port        = typeof Q.core.vars.port        !== 'undefined' ? Q.core.vars.port        : 23
Q.core.vars.binaryType  = typeof Q.core.vars.binaryType  !== 'undefined' ? Q.core.vars.binaryType  : 'arraybuffer'
Q.core.vars.subprotocol = typeof Q.core.vars.subprotocol !== 'undefined' ? Q.core.vars.subprotocol : false
Q.core.vars.formalClose = typeof Q.core.vars.formalClose !== 'undefined' ? Q.core.vars.formalClose : true

Q.core.vars.connected   = typeof Q.core.vars.connected   !== 'undefined' ? Q.core.vars.connected   : false
Q.core.vars.lines       = typeof Q.core.vars.lines       !== 'undefined' ? Q.core.vars.lines       : 0
Q.core.vars.packets     = typeof Q.core.vars.packets     !== 'undefined' ? Q.core.vars.packets     : 0
Q.core.vars.reloadCount = typeof Q.core.vars.reloadCount !== 'undefined' ? Q.core.vars.reloadCount : 0
Q.core.vars.reloadList  = typeof Q.core.vars.reloadList  !== 'undefined' ? Q.core.vars.reloadList  : []
Q.core.vars.reloadTime  = typeof Q.core.vars.reloadTime  !== 'undefined' ? Q.core.vars.reloadTime  : ''

Q.core.vars.scrollback       = typeof Q.core.vars.scrollback       !== 'undefined' ? Q.core.vars.scrollback       : false
Q.core.vars.scrollbackOffset = typeof Q.core.vars.scrollbackOffset !== 'undefined' ? Q.core.vars.scrollbackOffset : 60
Q.core.vars.sessionTime      = typeof Q.core.vars.sessionTime      !== 'undefined' ? Q.core.vars.sessionTime      : null
Q.core.vars.wrapPage         = typeof Q.core.vars.wrapPage         !== 'undefined' ? Q.core.vars.wrapPage         : false
Q.core.vars.wrapPre          = typeof Q.core.vars.wrapPre          !== 'undefined' ? Q.core.vars.wrapPre          : ''
Q.core.vars.wrapSequence     = typeof Q.core.vars.wrapSequence     !== 'undefined' ? Q.core.vars.wrapSequence     : 1

Q.core.vars.performance      = typeof Q.core.vars.performance      !== 'undefined' ? Q.core.vars.performance      : 0
Q.core.vars.performanceFlag  = typeof Q.core.vars.performanceFlag  !== 'undefined' ? Q.core.vars.performanceFlag  : false
Q.core.vars.ping             = typeof Q.core.vars.ping             !== 'undefined' ? Q.core.vars.ping             : 0
Q.core.vars.pinging          = typeof Q.core.vars.pinging          !== 'undefined' ? Q.core.vars.pinging          : false
Q.core.vars.pingInterval     = typeof Q.core.vars.pingInterval     !== 'undefined' ? Q.core.vars.pingInterval     : 180 * 1000

Q.core.vars.commandHistory   = typeof Q.core.vars.commandHistory   !== 'undefined' ? Q.core.vars.commandHistory   : []
Q.core.vars.commandPosition  = typeof Q.core.vars.commandPosition  !== 'undefined' ? Q.core.vars.commandPosition  : 0